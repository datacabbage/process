(function(){
	function random(min,max){
		return Math.floor(min+Math.random()*(max-min));
	}
	function checkAlert(position){
		var m=$("#"+position);
		if(m.length==0){
			if(position=='topRightAlert'){
				var div="<div id='"+position+"' style='position: fixed;top: 10px;right: 10px;z-index: 9999;width:360px;'></div>";
				$("body").append(div);
			}else if(position=='bottomRightAlert'){
				var div="<div id='"+position+"' style='position: fixed;bottom: 0px;right: 10px;z-index: 9999;width:360px;'></div>";
				$("body").append(div);
			}
			
		}		
	}
	$.alert=function(data){
		var id="alert"+random(1000,9999);
		if(!data.position){
			data.position='topRight';
		}
		checkAlert(data.position+"Alert");
		var str="<div id='"+id+"' class='alert "+data.style+"'>";
		if(data.closeBtn==undefined){
			data.closeBtn=true;
		}	
		if(data.closeBtn=='false'){
			data.closeBtn=false;
		}
		if(data.closeBtn){
			str+="<a href='#' class='close' data-dismiss='alert'>&times;</a>";
		}		
		data.content=data.content+"&nbsp;&nbsp;&nbsp;&nbsp;"
		str+="<strong>"+data.title+"！</strong>"+data.content+"</div>";
		$("#"+data.position+"Alert").prepend(str);
		if(data.autoClose==undefined){
			data.autoClose=true;
		}
		if(data.autoClose=='false'){
			data.autoClose=false;
		}
		if(data.autoClose){
			setTimeout(function(){
				$("#"+id).fadeOut(1000,function(){
					$("#"+id).remove();
					var m=$("#topRightAlert>div");
					if(m.length==0){
						$("#topRightAlert").remove();
					}
					var l=$("#bottomRightAlert>div");
					if(l.length==0){
						$("#bottomRightAlert").remove();
					}
				});				
			},3000);
		}
	}
})();
