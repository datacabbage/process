package cn.lcfms.bin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//综合缓存类
public class BaseCache {
	//后台栏目缓存
    public static List<HashMap<String, Object>> ITEMCACHE=new ArrayList<>();
    //生成静态列表
    public static List<HashMap<String, Object>> HTML=new ArrayList<>();
    //表信息
    public static HashMap<String,List<HashMap<String, Object>>> TABLEINFO=new HashMap<>();
    //权限列表
    public static List<HashMap<String, Object>> PERMIT=new ArrayList<>();
}
