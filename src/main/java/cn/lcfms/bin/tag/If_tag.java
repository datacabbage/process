package cn.lcfms.bin.tag;

import java.util.Stack;

import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

public class If_tag extends TagSupport{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean flag = false;
	private boolean test;
    public boolean isTest() {
		return test;
	}

	public void setTest(boolean test) {
		this.test = test;
	}
	/**
     * @return int
     */
    @Override
    public int doStartTag() {
        Stack<Object> stack = getStack(this.pageContext, true);
        stack.push(this);
        if(this.test == true) {
            this.finish();
            return EVAL_BODY_INCLUDE;
        }
        else {
        	this.unfinish();
            return SKIP_BODY;
        }
    }

    /**
     * @param pageContext
     * @return IfTag
     */
    public static If_tag getIfTag(PageContext pageContext) {
        Stack<Object> stack = getStack(pageContext, false);

        if(stack == null) {
            return null;
        }
        else {
            return (If_tag)(stack.peek());
        }
    }

    /**
     * @param pageContext
     */
    public static void remove(PageContext pageContext) {
        Stack<Object> stack = getStack(pageContext, false);

        if(stack != null && stack.size() > 0) {
            stack.pop();
        }
    }

    /**
     * @param pageContext
     * @param create
     * @return Stack<Object>
     */
    @SuppressWarnings("unchecked")
    public static Stack<Object> getStack(PageContext pageContext, boolean create) {
        Stack<Object> stack = (Stack<Object>)(pageContext.getAttribute("ifStack"));
        if(stack == null && create) {
            stack = new Stack<Object>();
            pageContext.setAttribute("ifStack", stack);
        }
        return stack;
    }

    /**
     * @return boolean
     */
    public boolean complete() {
        return this.flag;
    }

    /**
     *
     */
    public void finish() {
        this.flag = true;
    }
    
    public void unfinish(){
    	this.flag=false;
    }
}
