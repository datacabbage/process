package cn.lcfms.app.process.controller;

import java.io.File;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.lcfms.app.process.service.ProcessEditService;
import cn.lcfms.bin.core.App;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller("process.IndexController")
public class IndexController{
	
	@Autowired
	private ProcessEditService ps;
	
	@RequestMapping("/init")
	public ModelAndView init(HttpServletRequest request,HttpServletResponse response){
		String contextPath = request.getContextPath();
	    String basePath = "";
	    if(request.getServerPort()==80){
	    	basePath = request.getScheme()+"://"+request.getServerName()+contextPath+"/";
	    }else{
	    	basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
	    }	
    	request.setAttribute("APP", basePath); 
    	request.setAttribute("JS", basePath+"statics/ace/js/"); 
    	request.setAttribute("CSS", basePath+"statics/ace/css/"); 
    	request.setAttribute("IMG", basePath+"statics/ace/images/"); 
    	ModelAndView view=new ModelAndView("process/init");	
		return view;
	}	
	
	@RequestMapping("/selectUser")
	public String selectUser(String keywords) {
		JSONArray array=new JSONArray();
		return array.toString();
	}
	
	@RequestMapping("/selectGroup")
	public String selectGroup(String keywords) {
		JSONArray array=new JSONArray();
		return array.toString();
	}
	
	@RequestMapping("/selectDePart")
	public String selectDePart(String keywords) {
		JSONArray array=new JSONArray();
		return array.toString();
	}
	
	@RequestMapping(value="/synchroniz",produces="application/json;charset=utf-8")
	@ResponseBody
	public String synchroniz(
			@RequestParam(defaultValue="",required=false) String xml,
			@RequestParam(defaultValue="",required=false) String type,
			@RequestParam(defaultValue="",required=false) String id,
			@RequestParam(defaultValue="",required=false) String value,
			String act			
			) throws IOException, DocumentException
	{		
		//初始化
		if(act.equals("start")) {
			File f1=new File(App.CONF_PATH+App.F+"bpmn"+App.F+"start.bpmn");
			String str1 = FileUtils.readFileToString(f1, "utf-8");
			JSONObject object=new JSONObject();
			object.put("xml", str1);
			return object.toString();
		}
		//获取流程基础信息
		if(act.equals("process")) {
			JSONObject obj = ps.getBaseData(xml);
			return obj.toString();
		}
		//根据id获取信息
		if(act.equals("show")) {
			JSONObject data = ps.getDataById(xml, id);
			return data.toString();
		}
		//修改Attribute属性
		if(act.startsWith("changeAttribute")) {
			String x;
			if(type.equals("process")) {
				x=ps.changeProcessName(xml, value);
			}else {
				String attribute=act.substring(16, act.length()-1);
				x=ps.changeAttribute(xml, id, type,attribute, value);
			}	
			JSONObject data = ps.getDataById(x, id);
			data.put("xml", x);
			return data.toString();
		}
		//修改子标签属性
		if(act.startsWith("changeChildAttribute")) {
			String attribute=act.substring(21, act.length()-1);
			String x=ps.changeChildAttribute(xml, id, type, attribute, value);
			JSONObject data = ps.getDataById(x, id);
			data.put("xml", x);
			return data.toString();
		}
		//修改开始事件
		if(act.equals("changeStartEvent")) {
			String x=ps.changeStartEvent(xml, id,value);
			JSONObject data = ps.getDataById(x, id);
			data.put("xml", x);
			return data.toString();
		}
		//修改结束事件
		if(act.equals("changeEndEvent")) {
			String x=ps.changeEndEvent(xml, id,value);
			JSONObject data = ps.getDataById(x, id);
			data.put("xml", x);
			return data.toString();
		}
		//修改任务
		if(act.equals("changeTask")) {
			String x=ps.changeEventTask(xml, id,value);
			JSONObject data = ps.getDataById(x, id);
			data.put("xml", x);
			return data.toString();
		}
		//修改网关
		if(act.equals("changeGateway")) {
			String x=ps.changeEventTask(xml, id,value);
			JSONObject data = ps.getDataById(x, id);
			data.put("xml", x);
			return data.toString();
		}		
		//修改网关的条件表达式
		if(act.equals("changeConditionExpression")) {
			String x=ps.changeConditionExpression(xml, id,value);
			JSONObject data = ps.getDataById(x, id);
			data.put("xml", x);
			return data.toString();
		}		
		//修改process的命名空间
		if(act.equals("changeNamespace")) {
			String x=ps.changeNamespace(xml, value);
			JSONObject data = ps.getDataById(x, id);
			data.put("xml", x);
			return data.toString();
		}
		//修改文档
		if(act.equals("changeDocument")) {
			String x=ps.changeDocument(xml, type, id, value);
			JSONObject data = ps.getDataById(x, id);
			data.put("xml", x);
			return data.toString();
		}
		//保存表单
		if(act.equals("saveForm")) {
			String x=ps.saveForm(xml, type, id, value);
			JSONObject data = ps.getDataById(x, id);
			data.put("xml", x);
			return data.toString();
		}
		//保存表单
		if(act.equals("savepermission")) {
			String x=ps.savepermission(xml, type, id, value);
			JSONObject data = ps.getDataById(x, id);
			data.put("xml", x);
			return data.toString();
		}
		//修改中间事件
		if(act.equals("changeIntermediate")) {
			String x=ps.changeIntermediate(xml, id, value);
			JSONObject data = ps.getDataById(x, id);
			data.put("xml", x);
			return data.toString();
		}
		JSONObject data = new JSONObject();
		data.put("xml", xml);
		return data.toString();
	}	
	
	@RequestMapping(value="/save",produces="text/xml;charset=utf-8")
	@ResponseBody
	public String save(HttpServletRequest request) throws IOException{		
		return "保存成功！";
	}

}
