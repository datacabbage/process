package cn.lcfms.utils;

public class StringUtils {
	/**
	 * 将字符串转换为数字
	 * @param data 字符串
	 * @param df 如果为空设置默认
	 * @return
	 */
	public static int StringToInteger(String data,int df){
		if(null==data){
			return df;
		}else{
			try {
				return Integer.valueOf(data);
			} catch (Exception e) {
				return df;
			}
			
		}		
	}
	/**
	 * 分割字符串数组
	 * @param strs 字符串数组
	 * @param sp 分隔符
	 * @return
	 */
	public static String join(String[] strs,String sp) {
		StringBuffer r=new StringBuffer();
		for(int i=0;i<strs.length;i++) {
			r.append(strs[i]);
			if(i!=strs.length-1) {
				r.append(sp);
			}
		}
		return r.toString();
	}
}
